+++
title = "RADIO SHOWS"
+++

I host a regular slot on a radio show produced by my friend Prof. Murray and historically broadcast on [Gaga Radio](https://www.gagaradio.org), though it is now [self published](https://www.mixcloud.com/Psychojazzologist/). The show is called Clear A Space, and is a musical journey accompanying a lighthearted exploration of life and emotions. My section is Wait For The Moment and comprises a selection of some of my favourite jazz/funk/soul/hip-hop tunes revolving around a central emotion/vibe. Check out the episodes below.
