+++
title = "Episode 15"
date = "2022-12-01"
show = "Psychojazzologist%2Fclear-a-space-with-prof-murray-015"
+++

Life can get overwhelming huh? Join me in waiting for that overwhelming moment with the help of Corto Alto, Nick Hakim and - of course - Vulfpeck. Wait for the Moment starts at 11:06.