+++
title = "Episode 14"
date = "2022-10-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-014"
+++

After a holiday from the show and from life, I have some holiday-centric tunes to share. Featuring the likes of Antonio Carlos Jobim, Takuya Kuroda, Vieux Farka Touré and a very special guest band. Wait For The Moment starts at 1:14:40.