+++
title = "Episode 8"
date = "2021-12-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-008"
+++

It's the final Clear A Space of the year! Wait For The Moment starts at 1:12:58 and I'm thinking about finality and endings, with the help of tracks from William Devaughn, Monophonics and Portico Quartet, among others.