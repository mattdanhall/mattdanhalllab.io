+++
title = "Episode 19 - Christmas Special"
date = "2023-12-24"
show = "Psychojazzologist%2Fclear-a-space-with-prof-murray-xmas-special-2023"
+++

'Tis the season - and I've got some of jazzy Christmas joy for you to indulge in. Featuring holiday tunes from Woody Goss, Louis Armstrong, Laufey and Vince Guaraldi.
