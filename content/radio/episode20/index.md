+++
title = "Episode 20"
date = "2024-04-21"
show = "Psychojazzologist%2Fclear-a-space-with-prof-murray-episode-20"
+++

Clear A Space is back - the first episode of my 30s. I'm feeling old, so here's some music to contemplate aging to. Including songs by Oscar Jerome, Mulatu Astatke and SAULT. Wait For The Moment starts at 1:07:15.