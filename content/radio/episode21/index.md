+++
title = "Episode 21"
date = "2024-10-18"
show = "Psychojazzologist%2Fprof-murray-with-dr-matt-and-rockin-ronnie-clear-a-space-show-21"
+++

Feeling "blissed out" after a trip to Winterton Beach has inspired this episode. Featuring tunes from DARGZ, Zé Roberto and Abstract Orchestra, to name a few. Wait For The Moment starts at 10:21.
