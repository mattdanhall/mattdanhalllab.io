+++
title = "Episode 9"
date = "2022-02-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-009"
+++

Clear A Space is back for 2022! As well as the first of the year, it's the first episode since submitting my thesis, so Wait For The Moment is focussed on celebrating. It starts at 1:04:47 and features music from Marva Whitney, Edoardo Vianello and Mr Jukes, to name a few.