+++
title = "Episode 6"
date = "2021-07-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-006"
+++

Inspired by Prof. Murray on last month's Clear A Space, I'm in a sentimental mood on this months Wait For The Moment. It starts at 1:04:48 and features tracks from Donny Hathaway, Durand Jones & The Indications and Dinner Party.