+++
title = "Episode 1"
date = "2021-02-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-001"
+++

Welcome to the first Clear A Space! Wait For The Moment is in two parts, starting at 28:51 and 1:26:45. It is themed around feelings of optimism in part 1 and realism/uncertainty in part 2. Feturing tracks from Cory Wong, The Hot Sardines and BADBADNOTGOOD, among others.