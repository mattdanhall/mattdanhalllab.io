+++
title = "Episode 18"
date = "2023-10-01"
show = "Psychojazzologist%2Fclear-a-space-with-prof-murray-018"
+++

This episode I've got music to heist to. Nyah-rock from Cymande, funk from Curtis Mayfield and Jungle Jazz from Kool & the Gang set the mood. Wait for the Moment starts at 4:41.
