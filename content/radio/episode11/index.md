+++
title = "Episode 11"
date = "2022-05-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-011"
+++

This time round I'm feeling mysterious - thanks to Steve for the inspiration! If anyone has a musical moment for me, get in touch via [Twitter](https://twitter.com/gaga_radio_uk) or [the website](https://gagaradio.org/contact/). The episode features music from Joomanji, Pino Palladino & Blake Mills, Kokoroko and Alice Coltrane & Pharoah Sanders. It starts at 1:00:21.