+++
title = "Episode 4"
date = "2021-05-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-004"
+++

Wait For The Moment starts at 35:26 and deals with the feelings of regret and remorse. Featuring tracks from Billie Holiday, Joey Dosik and Jordan Rakei, among others.