+++
title = "Episode 12"
date = "2022-07-01"
show = "gagaradio%2Fclear-a-space-with-prof-murray-012"
+++

Hunger has kicked in, and I'm looking for a tasty tune or two to sate it. On this episode of Wait For The Moment I've deliciousness from Louie Ramirez, Louis Prima and Louis Armstrong, among others, starting at 20:14.