+++
title = "ABOUT"
type = "about"
+++

_**I like food, music, tech, and the occasional beer or two.**_

A little about me:
* I work as a software engineer.
* I have a doctorate in robotic control, as a member of the [Natural Robotics Lab](https://www.sheffield.ac.uk/naturalrobotics) at the University of Sheffield.
* [My research](./publications) is concerned with novel methods of self-reconfiguration for modular robotic systems.
* I have been known to [procrastinate](./projects).
* You can hear some music I like on my guest radio slot, [Wait For The Moment](./radio), that I produce for my friend Prof. Murray's show called [Clear a Space](https://www.mixcloud.com/Psychojazzologist/).
* You can hear even more music I like on my [Spotify monthly archive](https://open.spotify.com/user/mattdanhall/playlists).
* Reach out on [LinkedIn](https://www.linkedin.com/in/matthew-d-hall) or via [email](mailto:mattdanhall@duck.com) if you want a chat.
