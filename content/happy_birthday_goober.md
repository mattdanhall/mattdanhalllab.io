+++
title = "Happy Birthday Goober"
+++

30 things that you make infinitely better (in no particular order):

1. Eating all the best food
2. Cooking even better food
3. Going to the pub for a random pint
4. Listening to records
5. Going to see live music
6. Dancing, both in the kitchen and at parties
7. Swimming in the ice cold sea
8. Playing Mario Kart
9. Hosting friends for dinner
10. [Going to the cinema](https://memberships.picturehouses.com/subscription/member-sign-in)
11. Being in bed playing connections before nodding off
12. _Being_ in bed all morning on lazy days
13. Baths
14. Weddings
15. Hiking trips
16. Mid-day walks on the Rye
17. Sitting on the sofa
18. Exploring new parts of the city
19. Exploring new cities
20. Playing board games
21. Work from home days together with regular cuppas
22. Hanging up the washing
23. Mad holidays
24. Watching rugby (when you teach me the rules)
25. Hanging out with our families
26. Getting a takeaway
27. Taking the train (with a train picnic)
28. Cuddles
29. Almost dying from dislodged wheel nuts
30. EVERYTHING!
