+++
image = "poster.png"
title = "Shape Formation via Active Subtraction with Modular Robots"
pub = "ACSE Researcher Symposium"
date = "2019-02-21"
type = "gallery"
+++

## Presented at the ACSE Researcher Symposium 2019.

<!--more-->
_M. D. Hall_.

Awarded 'Best Poster Presentation'.