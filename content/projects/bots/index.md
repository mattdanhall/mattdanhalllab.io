+++
title = "Bots"
date = "2025-02-02"
link = "https://mattdanhall.itch.io/bots"
+++

A game created for Big Mode Game Jam 2025, in collaboration with Liam Burton. The first we had ever made!

The jam theme was "power" - our submission is a puzzle platformer which involves using dormant bots to transfer power and control through a derelict factory.

Couldn't be prouder of how it came out for a first game.
