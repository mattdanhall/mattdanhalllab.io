+++
title = "Pom Time"
date = "2022-01-01"
link = "https://gitlab.com/mattdanhall/pom-time"
+++

Pom Time is a minimalist pomodoro timer for the command line, written in bash.