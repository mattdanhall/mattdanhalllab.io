+++
title = "PROJECTS"
+++

Here a few little side projects and tinkerings that I've worked on over the years. Some available to view on my [GitLab](https://www.gitlab.com/mattdanhall).