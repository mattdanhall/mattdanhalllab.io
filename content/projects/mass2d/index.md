+++
title = "MASS2D"
date = "2021-10-01"
link = "https://gitlab.com/natural-robotics-lab/mass2d"
+++

MASS2D is the Modular Active Subtraction Simulator in 2 Dimensions. Developed as part of my PhD, it simulates the self-reconfiguration algorithms I created and can visualise the results. Written in Python 3.